class UsersService {
    constructor($resource, $http, $q, CONFIG) {
        'ngInject'; //Inject necessario para que o angular faca as injecoes as factorys e services
        this.$resource = $resource;
        this.$http = $http;
        this.$q = $q;
        this.CONFIG = CONFIG;
        this.urlApiCustom = this.CONFIG.urlApi + 'users';
    }

    getListUsers() {
        const defer = this.$q.defer();

        const resource = this.$resource(this.urlApiCustom, {});

        resource.get().$promise.then((response) => {
            defer.resolve(response.users);
        }, (error) => {
            defer.reject(error);
        });

        return defer.promise;
    };

    getRegisterUser(dataUser) {
        const defer = this.$q.defer();

        const resource = this.$resource(this.urlApiCustom, dataUser);

        resource.save().$promise.then((response) => {
            defer.resolve(response);
        }, (error) => {
            defer.reject(error);
        });

        return defer.promise;
    }

    getEditUser(dataUser) {
        const defer = this.$q.defer();

        const resource = this.$resource(
            this.urlApiCustom +'/:id',
            dataUser,
            {
                'update': {method: 'PUT'},
                params: {id: dataUser.id}
            });

        resource.save(dataUser).$promise.then((response) => {
            defer.resolve(response);
        }, (error) => {
            defer.reject(error);
        });

        return defer.promise;
    }

    removeUsers(userId) {
        const defer = this.$q.defer();

        const resource = this.$resource(this.urlApiCustom,);

        resource.remove({id: userId}).$promise.then((response) => {
            defer.resolve(response.data);
        }, (error) => {
            defer.reject(error.data);
        });

        return defer.promise;
    };
}

export default UsersService;