const configs = angular.module('myApp.configs', [])
    .constant('CONFIG', {
        urlApi: 'http://transporco.com.br/api/',
        tokenApi: '12345',
        tokenTypeAlg: 'HS512',
    });

export default configs;
