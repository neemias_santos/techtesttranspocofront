class HomeController {
    constructor($scope, $rootScope, $http, UsersService, $resource, $filter) {
        'ngInject';
        this.name = 'HomeController';
        this.$http = $http;
        this.$scope = $scope;
        this.$resource = $resource;
        this.$filter = $filter;
        this.$rootScope = $rootScope;
        this.UsersService = UsersService;
        this.dataUser = {};
        this.msgAlert = [];

        this.searchUsers();

    }

    searchUsers() {
        this.UsersService.getListUsers().then((response) => {
            this.$scope.users = response;
        }, (error) => {
        }).finally(() => {
        });
    }

    newUser(dataUser) {

        if (!dataUser.id && dataUser.name && dataUser.mail) {
            this.UsersService.getRegisterUser(dataUser).then((response) => {
                console.log(response);

                if (response.type === 1) {
                    this.msgAlert.type = 'success';
                    this.msgAlert.text = response.data;
                    this.$scope.dataUser = {};
                }
                this.searchUsers();
            }, (error) => {
                if (error.data.type === 2) {
                    this.msgAlert.type = 'warning';
                    this.msgAlert.text = error.data.data;
                }
            }).finally(() => {
            });
        }else{
            this.UsersService.getEditUser(dataUser).then((response) => {
                console.log(response);

                if (response.type === 1) {
                    this.msgAlert.type = 'success';
                    this.msgAlert.text = response.data;
                    this.$scope.dataUser = {};
                }
                this.searchUsers();
            }, (error) => {
                if (error.data.type === 2) {
                    this.msgAlert.type = 'warning';
                    this.msgAlert.text = error.data.data;
                }
            }).finally(() => {
            });
        }
    }

    editUser(dataUser) {
        this.$scope.dataUser = dataUser;
    }

    removeUser(userId) {
        this.UsersService.removeUsers(userId).then((response) => {

            if (response.type === 1) {
                this.msgAlert.type = 'success';
                this.msgAlert.text = response.data;
                this.$scope.dataUser = {};
            }
            this.searchUsers();
        }, (error) => {
            if (error.data.type === 2) {
                this.msgAlert.type = 'warning';
                this.msgAlert.text = error.data.data;
            }
        }).finally(() => {
        });
    }


}

export default HomeController;